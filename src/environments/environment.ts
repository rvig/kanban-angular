// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDfC-aPIhAZoKd08n3Zf_KyUecvEsRi3qU",
    authDomain: "angular-kanban-f2bcf.firebaseapp.com",
    databaseURL: "https://angular-kanban-f2bcf.firebaseio.com",
    projectId: "angular-kanban-f2bcf",
    storageBucket: "angular-kanban-f2bcf.appspot.com",
    messagingSenderId: "5532748451",
    appId: "1:5532748451:web:177259fe5baa8ec5a5cd16",
    measurementId: "G-4704T8F3FB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
